package fr.formation.vo;

import java.util.Map;

public class PersonneVo {

	private String nom;
	private String prenom;
	private String email;
	private String mdp;
	private String confirmationMdp;
	private int id;
	private boolean isFormateur;
	
	
	//TODO
	private Map<String,String> mapErreur;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public boolean isFormateur() {
		return isFormateur;
	}
	public void setFormateur(boolean isFormateur) {
		this.isFormateur = isFormateur;
	}
	public String getConfirmationMdp() {
		return confirmationMdp;
	}
	public void setConfirmationMdp(String confirmationMdp) {
		this.confirmationMdp = confirmationMdp;
	}
	@Override
	public String toString() {
		return "PersonneVo [nom=" + nom + ", prenom=" + prenom + ", email="
				+ email + ", mdp=" + mdp + ", confirmationMdp="
				+ confirmationMdp + ", id=" + id + ", isFormateur="
				+ isFormateur + "]";
	}

	
	
	
	
	
	
}
