package fr.formation.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

//import com.mysql.jdbc.Connection;
//import com.mysql.jdbc.PreparedStatement;

import fr.formation.buisness.beans.Formation;
import fr.formation.buisness.beans.PersonneDao;



public class DaoImpl implements IDao{
	
	
	 private static DaoImpl dao = null;
//	 private Connection connection = (Connection) DBUtil.getConnection();
	 private Connection connection = (Connection) DBUtilWithDataSource.getConnection();
	 
	@Override
	public PersonneDao findPersonneByUtilisateur(PersonneDao p) {
		 PersonneDao user = null;
        try {
        	
            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("select * from personne where email=? and mdp=?");
            preparedStatement.setString(1, p.getEmail());
            preparedStatement.setString(2, p.getMdp());
            ResultSet rs = preparedStatement.executeQuery();
            
            
           
            while (rs.next()) {
            	user = new PersonneDao();
                user.setId (rs.getInt("id"));
                user.setNom (rs.getString("nom"));
                user.setPrenom(rs.getString("prenom"));
                user.setEmail(rs.getString("email"));
                user.setFormateur(rs.getBoolean(6));
               
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return user;

	}
	
	
	
	
	public static DaoImpl getInstance(){
		
		if(dao==null){
			dao = new DaoImpl();
		}
		
		return dao;
	}

	@Override
	public List<Formation> getListeFormationParFormateur(int id) {
		 List <Formation> listeFormation = new ArrayList<Formation>();
	        try {
	            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("select Count(fp.id_formation) AS nb, f.id , f.label,f.description FROM  formation_personne fp, formation f " +
	            		"																				where f.id=fp.id_formation and f.id in (" +
	            		"																							select ff.id_formation from formation_personne ff, personne p " +
	            		"																							where p.id=ff.id_personne and ff.id_personne=? ) " +
	            		"																				GROUP BY fp.id_formation");
	            preparedStatement.setInt(1, id) ;
	            ResultSet rs = preparedStatement.executeQuery();
	            
	            while (rs.next()) {
	            	Formation formation = new  Formation();
	            	formation.setId(rs.getInt("id"));
	                formation.setLabel(rs.getString("label"));
	                formation.setDescription(rs.getString("description"));
	                formation.setNombreParticipant(rs.getInt("nb") -1);
	                listeFormation.add(formation);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
			return listeFormation;
		
	}
	
	@Override
	public List<Formation> getListeFormationParPersonne(int id) {
		 List <Formation> listeFormation = new ArrayList<Formation>();
	        try {
	        	
//	        	SELECT Count(fp.id_formation) AS nb, f.id , f.label,f.description FROM  formation_personne fp, formation f where f.id=fp.id_formation 
//	        			and f.id in (select ff.id_formation from formation_personne ff, personne p where p.id=ff.id_personne and ff.id_personne=1 )	    	        	GROUP BY fp.id_formation	        	
	            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("select f.id,f.label,f.description from formation f,formation_personne fp" +
	            																						" where fp.id_personne=? and f.id=fp.id_formation");
	            preparedStatement.setInt(1, id) ;
	            ResultSet rs = preparedStatement.executeQuery();
	            
	           
	            while (rs.next()) {
	            	Formation formation = new  Formation();
	            	formation.setId(rs.getInt("id"));
	                formation.setLabel(rs.getString("label"));
	                formation.setDescription(rs.getString("description"));
	                listeFormation.add(formation);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
			return listeFormation;
		
	}
	
	
	public void creationFormation(Formation f){
		PreparedStatement preparedStatement=null;
		try {
             preparedStatement = (PreparedStatement) connection.prepareStatement("insert into FORMATION(label,description) values (?, ? )");
            // Parameters start with 1
            preparedStatement.setString(1, f.getLabel());
            preparedStatement.setString(2, f.getDescription());
            preparedStatement.executeUpdate();
            
            int idFomation =0;
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs != null && rs.next()) {
            	idFomation = rs.getInt(1);
            }
            
            preparedStatement = (PreparedStatement) connection.prepareStatement("insert into FORMATION_PERSONNE(id_personne,id_formation) values (?, ? )");
            preparedStatement.setInt(1, f.getIdFormateur()) ;
            preparedStatement.setInt(2, idFomation) ;
            preparedStatement.executeUpdate();
            
            
            preparedStatement.close();
		
		} catch (SQLException e) {
            throw new DaoException(e.getMessage(), 1);
        }
	}

	@Override
	public  List<Formation> recupererAllFormation() {
		 List <Formation> listeFormation = new ArrayList<Formation>();
	        try {
	            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("select * from formation");
	            ResultSet rs = preparedStatement.executeQuery();
	            
	           
	            while (rs.next()) {
	            	Formation formation = new  Formation();
	            	formation.setId(rs.getInt("id"));
	                formation.setLabel(rs.getString("label"));
	                formation.setDescription(rs.getString("description"));
	                listeFormation.add(formation);
	                
	                preparedStatement.close();
	                connection.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
			return listeFormation;
	}

	@Override
	public void inscriptionFormation(int idPersonne, int idFormation) {
		PreparedStatement preparedStatement=null;
		try {
            
            preparedStatement = (PreparedStatement) connection.prepareStatement("insert into FORMATION_PERSONNE(id_personne,id_formation) values (?, ? )");
            preparedStatement.setInt(1, idPersonne) ;
            preparedStatement.setInt(2, idFormation) ;
            preparedStatement.executeUpdate();
            
            preparedStatement.close();
		
		} catch (SQLException e) {
            throw new DaoException(e.getMessage(), 1);
        }
		
	}
	

	@Override
	public  PersonneDao createPersonne(PersonneDao personne) {
		PreparedStatement preparedStatement=null;
			try {
				//cas sans datasource
//	             preparedStatement = (PreparedStatement) connection.prepareStatement("insert into PERSONNE(NOM,PRENOM,EMAIL,MDP) values (?, ?, ?, ? )");

				preparedStatement = (PreparedStatement) connection.prepareStatement("insert into PERSONNE(NOM,PRENOM,EMAIL,MDP) values (?, ?, ?, ? )",Statement.RETURN_GENERATED_KEYS);
	            // Parameters start with 1
	            preparedStatement.setString(1, personne.getNom());
	            preparedStatement.setString(2, personne.getPrenom());
	            preparedStatement.setString (3, personne.getEmail());
	            preparedStatement.setString (4, personne.getMdp());
	            preparedStatement.executeUpdate();
	           
	            int id =0;
	            ResultSet rs = preparedStatement.getGeneratedKeys();
	            if (rs != null && rs.next()) {
	                id = rs.getInt(1);
	            }
	            
	            System.out.println(" lid genere " + id );
	          
	            personne.setId(id);
	            rs.close();
	            preparedStatement.close();
//	            connection.close();
			
			} catch (SQLException e) {
	            throw new DaoException(e.getMessage(), 1);
	        }
			return personne;
		
	}
	
	public List<PersonneDao> getAllPersonnes(){
		List<PersonneDao> liste = new ArrayList<PersonneDao>();
		  try {
	            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement("select * from personne");
	            ResultSet rs = preparedStatement.executeQuery();
	            
	           
	            while (rs.next()) {
	            PersonneDao user = new PersonneDao();
	                user.setId (rs.getInt("id"));
	                user.setNom (rs.getString("nom"));
	                user.setPrenom(rs.getString("prenom"));
	                user.setEmail(rs.getString("email"));
	            
	                liste.add(user);
	               
	            }
	            rs.close();
	            preparedStatement.close();
//                connection.close();
	            
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
		return liste;
		
	}
	
}