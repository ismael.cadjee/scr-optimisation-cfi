package fr.formation.dao;


import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public class DBUtilWithDataSourceBis {

	private static Connection connection = null;
	private static  Context ctx = null;
	
	
	public static Connection getConnection() {
		if (connection != null)
			return connection;
		else {
			try {
				
				 ctx = new InitialContext();
		         DataSource ds = (DataSource) ctx.lookup("java:/dsFormationJSP");
		             
		            connection =  ds.getConnection();
		          
		            if(connection ==null){
		            	throw new RuntimeException("JNDI non initi�");
		            }
		            
				
			} catch (NamingException e) {
				throw new RuntimeException(e.getMessage());
			} catch(SQLException e){
				throw new RuntimeException(e.getMessage());
			}
			//
			return connection;
		}

	}
}
