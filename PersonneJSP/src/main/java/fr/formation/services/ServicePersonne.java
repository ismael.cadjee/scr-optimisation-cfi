package fr.formation.services;

import java.util.ArrayList;
import java.util.List;

import fr.formation.buisness.beans.PersonneDao;
import fr.formation.buisness.traitement.FactoryPersonne;
import fr.formation.dao.DaoImpl;
import fr.formation.dao.IDao;
import fr.formation.vo.PersonneVo;

public class ServicePersonne implements IServicePersonne {

	private IDao dao ;
	
	//TODO ICA  sortir le DAO 
	public ServicePersonne(IDao dao) {
		this.dao = dao;
	}
	
	
	public ServicePersonne() {
		this.dao  = DaoImpl.getInstance();
	}
	
	@Override
	public boolean isPersonneExiste(PersonneVo personne) {
		
		return false;
	}

	@Override
	public PersonneVo recupererPersonne(PersonneVo personne) {
		PersonneDao entity = dao.findPersonneByUtilisateur(FactoryPersonne.builPersonne(personne));
		
		return entity==null?null : FactoryPersonne.builPersonneVo(entity);
	}

	@Override
	public PersonneVo enregistrerPersonne(PersonneVo personne) {
		PersonneDao entity =  dao.createPersonne(FactoryPersonne.builPersonne(personne));
		
	 return FactoryPersonne.builPersonneVo(entity);
	}

	@Override
	public List<PersonneVo> getAllPersonne() {
		
		List<PersonneDao > personne = dao.getAllPersonnes();
		List<PersonneVo> listVo = new ArrayList<PersonneVo>();
		
		for(PersonneDao p :personne){
			PersonneVo vo = new PersonneVo();
			listVo.add( FactoryPersonne.builPersonneVo(p));
		}
		
		
		return listVo;
	}

}
