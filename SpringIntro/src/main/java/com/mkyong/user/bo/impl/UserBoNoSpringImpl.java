package com.mkyong.user.bo.impl;

import javax.inject.Named;

import com.mkyong.user.bo.UserBo;
import com.mkyong.user.bo.UserBoNoSpring;

@Named
public class UserBoNoSpringImpl implements UserBoNoSpring{
 
	public String getMessage() {
		
		return "JSF 2 + NO Spring Integration";
	
	}
 
}