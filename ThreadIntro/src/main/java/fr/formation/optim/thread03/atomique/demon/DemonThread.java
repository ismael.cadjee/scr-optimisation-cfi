package fr.formation.optim.thread03.atomique.demon;

/**
 * 
 * @author CADJEE Ismael 
 *
 */
public class DemonThread extends Thread{
	DemonThread(){
		//La jvm n'attend pas la fin d'un demon et l'arrete des lors
		//que les user thread ont fini. il a une priorit� faible 
		// au sein de la JVM ! 
		//on utilise donc les demon pour destaches non critiques !!!!!
		setDaemon(true);//changer la valeur ici  vrai 
	}
	
	public void run() {
        int count = 0;

        while (true) {
            System.out.println("coucou de DemonThread "+count++);

            try {
                sleep(5000);
            } catch (InterruptedException e) {
                // handle exception here
            }
        }
    }
}
