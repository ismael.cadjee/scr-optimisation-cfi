package fr.formation.optim.thread03.atomique;

class Test implements Runnable {
	   public void run() {
	      for (int i = 0; i < 10; i++) {
	    	  TestProcessusThread.entier.incrementAndGet();
	         System.out.println(Thread.currentThread().getName() + " - " + (TestProcessusThread.entier));         
	         try {
	            Thread.sleep(2000);
	          
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }
	      }
	   }
	}