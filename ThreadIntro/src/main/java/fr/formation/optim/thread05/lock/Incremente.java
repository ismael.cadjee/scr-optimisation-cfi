package fr.formation.optim.thread05.lock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Incremente {
   private int entier = 0;
   private Lock verrou = new ReentrantLock();
  
   public  void incremente(){
	  
	//  if( verrou.tryLock(1000L, TimeUnit.MILLISECONDS)){
	   try{
		   
		   verrou.lock();
	         //tout ce code est maintenant consid�r� comme atomique !
	         entier++;
	     }finally{
	         //ainsi, m�me s'il y a eu une interruption sur notre thread
	         //le verrou sera rel�ch�, dans le cas contraire
	         //tous les autres threads ne pourraient plus travailler ! 
	         verrou.unlock();
	      }
	  }
   
   public Integer get(){
	   synchronized(this){
      return entier;
	   }
   }
}
