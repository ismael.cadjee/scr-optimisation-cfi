package fr.formation.optim.thread03A.atomique.cancel;

/**
 * 
 * @author CADJEE Ismael 
 *
 */
class Compteur implements Runnable {
	
	private int max;
	
	Compteur(int max){
		this.max=max;
	}
	  
	public void run() {
	    while (!Thread.currentThread().isInterrupted()){
	    	
	    	 TestProcessusThread.entier.incrementAndGet();
	         System.out.println(Thread.currentThread().getName() + " - " + (TestProcessusThread.entier)); 
	         
	       
	         try {
	            Thread.sleep(2000);
	            if( TestProcessusThread.entier.get()>=max){
	            	cancel();
	            
	     
	            }
	         } catch (InterruptedException e) {
	        	 Thread.currentThread().interrupted() ;
	        	 
	         }
	      }
	   }
	
	
	 public  void cancel() {
		 System.err.println(Thread.currentThread().getName() +" thread cancel");
	       // interruption du thread courant, c'est-�-dire le n�tre
	      Thread.currentThread().interrupt() ;
	   }
	}