package fr.formation.optim.thread06.compte.lock;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Compte {
	
	 private AtomicLong solde = new AtomicLong(1000L);
	 private final long decouvert = -130L;

	 private Lock verrou = new ReentrantLock();

	 public void retrait(long montant){
	      verrou.lock();
	      try{
	         long avant = solde.get();
	         solde.set((avant - montant));
	         solde();
	      }finally{
	         verrou.unlock();
	      }
	   }
	 
	 public synchronized void solde(){
	      System.out.println("Solde actuel, dans " + Thread.currentThread().getName()
	                           + " : " +  solde.longValue());
	   }
	 
	 public void depot(long montant){
	     //totalement inutile car l'objet
		 //est deja synchroniser
		 synchronized(this){
	         long result = solde.addAndGet(montant);
	         solde();
	      }
	   }

	 public synchronized long getSolde(){
	      return solde.longValue();
	   }
	   
	   public long getDecouvert(){
	      return decouvert;
	   }
	 
}
