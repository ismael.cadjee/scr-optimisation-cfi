package fr.formation.optim.thread06.compte.lock;

public class Test {
	public static void main(String[] args) {
		Compte ceb = new Compte();
	      
	      //On cr�e deux threads de retrait
	      Thread t1 = new ThreadRetrait(ceb);
	      t1.start();
	      
	      Thread t2 = new ThreadRetrait(ceb);
	      t2.start();
	      
	      //et un thread de d�p�t
	      Thread t3 = new ThreadDepot(ceb);
	      t3.start();
	}
}
