package fr.formation.optim.thread02.exception;


import java.util.Random;

public class ThreadException extends Thread{

   public ThreadException(String name){
      setName(name);
      setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
   }
   public void run(){
      Random rand = new Random();
      while(true){
         System.out.println(" - " + getName());
         int cas = rand.nextInt(5);
         
         switch(cas){
            case 0 :
               int i = 10 / 0;
               break;
            case 1 :
               String str = "isma";
               double d = Double.parseDouble(str);
               break;  
            default : 
               System.out.println("aucune erreur...");
               break;
         }
      }
   }
   public static void main(String[] args){
	      
	      for(int i = 0; i < 6; i++){
	         Thread t = new ThreadException("Thread-" + i);
	         t.start();
	      }
	   }
}