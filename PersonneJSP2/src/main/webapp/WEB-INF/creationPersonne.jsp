<%@ page import="java.util.List, java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/xml" prefix="x" %>
<%@ page isELIgnored="false"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>formation Servlet formulaire et session</title>
</head>

<body>
	
   <c:import url="/menu.jsp" />
   
	<form method="post" action="creationPersonneMVC">
            <fieldset>
                <legend>Création personne</legend>
               <table>
               	 <tr>
                	<td>
						<label for="nom">Nom</label>
					</td>
					<td>
	                	<input type="text" id="nom" name="nom" value="${personneVo.nom}" size="20" maxlength="20" />
	                	<span class="erreur">${erreurs['nom']}</span>
	                </td>
	            </tr>
	            <tr> 
	              <td>
	                <label for="nom">Prenom</label>
	              </td>
	              <td>
	                <input type="text" id="prenom" name="prenom" value="${personneVo.prenom}" size="20" maxlength="20" />
	                <span class="erreur">${erreurs['prenom']}</span>
	              </td>
	            </tr>
	            <tr>
	              <td> 
	                <label for="email">Adresse email <span >*</span></label>
	              </td>
	              <td>
	                <input type="text" id="email" name="email" value="<c:out value="${personneVo.email}"/>" size="20" maxlength="60" />
	                <span class="erreur"><c:out value="${requestScope.erreurs.email}" /></span>
	              </td>
	            </tr>
	            <tr>
	              <td>
	                <label for="motdepasse">Mot de passe <span >*</span></label>
	              </td>
	              <td> 
	                <input type="password" id="motdepasse" name="motdepasse" value="" size="20" maxlength="20" />
	                <span class="erreur">${erreurs['confirmation']}</span>
	              </td>
	           </tr>
	           <tr>
				 <td>
	                <label for="confirmation">Confirmation du mot de passe <span >*</span></label>
	             </td>
	             <td>   
	               <input type="password" id="confirmation" name="confirmation" value="" size="20" maxlength="20" />
	             </td>
	            </tr>
	            <tr>
	             <td>
	                
	             </td>
	             <td>  
	             	<input type="submit" value="Inscription"  />
	             </td>
	             </tr>
               </table>
            </fieldset>
        </form>
	
	
</body>
</html>