<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false"%>

 <c:if test="${!empty sessionScope.sessionPersonne}">
  Vous êtes connecté(e) avec l'adresse : ${sessionScope.sessionPersonne.email} <br>
  <a href="<c:url value="/deconnexion" />">deconnexion</a>
  <a href="<c:url value="/private/private.jsp" />"> Accès à mon Espace </a>
 </c:if>

 <c:if test="${ empty sessionScope.sessionPersonne}">
 
		<a href="<c:url value="creationPersonneMVC" />">Créer votre profil</a>
		<a href="<c:url value="/visualisation" />"> visualiser tous les profils</a>
  </c:if>
  <br><br>
